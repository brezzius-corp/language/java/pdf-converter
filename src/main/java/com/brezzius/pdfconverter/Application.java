package com.brezzius.pdfconverter;

import com.aspose.pdf.Document;
import com.aspose.pdf.ExcelSaveOptions;
import com.aspose.pdf.SaveFormat;

import java.io.File;

public class Application {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Usage : java -jar pdfconverter DIRECTORY");
            System.exit(1);
        }

        File dir = new File(args[0]);
        File[] list = dir.listFiles();

        if (list != null) {
            for (File item : list) {
                if (item.isFile() && item.getName().endsWith(".pdf")) {
                    try (Document document = new Document(args[0] + "/" + item.getName())) {
                        ExcelSaveOptions saveOptions = new ExcelSaveOptions();
                        saveOptions.setFormat(ExcelSaveOptions.ExcelFormat.XLSX);

                        document.save(args[0] + "/" + item.getName() + ".xlsx", SaveFormat.Excel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
